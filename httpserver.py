# -*- coding: utf-8 -*-

# from _typeshed import Self
from http import server
from http.server import HTTPServer, BaseHTTPRequestHandler
import cgi
import pprint
# import cgitb


# cgitb.enable()
# cgitb.enable(display=0, logdir="C:\\Users\\malda\\Documents\\dev\\httpserver.log")

tasklist = ['Task 1', 'Task 2', 'Task 3']


class requestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path.endswith("/tasklist"):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()
            # self.wfile.write(self.path[1:].encode())
            output = ''
            output += '<html><body>'
            output += '<h1>Task list</h1>'
            output += "<h3><a href='/tasklist/new'>Ajout d'une nouvelle tache</a></h3>" 
            for task in tasklist:
                output += task 
                output += ' <a href="/tasklist/%s/remove"> [DELETE] </a>' % task
                output += '<br>'
            output += '</body></html>'
            self.wfile.write(output.encode())
        
        if self.path.endswith('/new' or self.path.endswith('/new/')):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()

            output = ''
            output += '<html><body>'
            output += '<h1> Add new task</h1>'
            output += '<br>'
            output += '<form method="POST" enctype="multipart/form-data" action="/tasklist/new">'
            output += '<input name="task" type="text" placeholder="Add new task">'
            output += '<input type="submit" placeholder="Add">'
            output += '</form>'
            output += '</body></html>'

            self.wfile.write(output.encode())


        if self.path.endswith('/remove'):
            listIDPath = self.path.split('/')[2]
            print(listIDPath)
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()

            output = ''
            output += '<html><body>'
            output += '<h1> Remove task %s</h1>' %listIDPath.replace("%20"," ")
            output += '<form method="POST" enctype="multipart/form-data" action="/tasklist/%s/remove">' % listIDPath
            output += '<input type="submit" value="Remove"></form>'
            output += '<a href="/tasklist">Cancel</a>'
            output += '<br>'
            output += '</body></html>'

            self.wfile.write(output.encode())

    def do_POST(self):
        if self.path.endswith('/new'):
            # Creating two variables, content-type and primary dictionnary
            ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
            pdict["boundary"] = bytes(pdict["boundary"], "utf-8")
            content_len = int(self.headers.get('content-length'))
            pdict['CONTENT-LENGTH'] = content_len
            if ctype == 'multipart/form-data':
                fields = cgi.parse_multipart(self.rfile, pdict)
                new_task = fields.get('task')
                tasklist.append(new_task[0])
                print('New task created : %s' % new_task)

            self.send_response(301)
            self.send_header('content-type', 'text/html')
            self.send_header('Location','/tasklist')
            self.end_headers()

        if self.path.endswith('/remove'):
            listIDPath = self.path.split('/')[2]
            ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
            if ctype == 'multipart/form-data':
                list_item = listIDPath.replace('%20',' ')
                tasklist.remove(list_item)
                print('Removing task : %s' % list_item)

                self.send_response(301)
                self.send_header('content-type', 'text/html')
                self.send_header('Location','/tasklist')
                self.end_headers()

def main():
    PORT = 8000
    server = HTTPServer(('', PORT), requestHandler)
    print('Server running on port %s' % PORT)
    server.serve_forever()


if __name__ == '__main__':
    main()